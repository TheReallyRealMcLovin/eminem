﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Eminem.WindowImages;

namespace Eminem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            System.Windows.Forms.Application.Restart();
        }

        private void b_defaultimage_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
            {
                Title = "Standartbild wählen",
                Filter = "JPG Files(*.jpg)|*.jpg|PNG Files(*.png)|*.png"
            };

            string CombinedPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\DefaultImages");
            dlg.InitialDirectory = System.IO.Path.GetFullPath(CombinedPath);
            
            if (dlg.ShowDialog() != null)
            {
                tb_speicherort.Text = System.IO.Path.Combine(CombinedPath, dlg.FileName).ToString();
            }
        }

        private void b_background_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog
            {
                Title = "Bild wählen",
                Filter = "PNG Files(*.png)|*.png|JPG Files(*.jpg)|*.jpg"
             };


            if (dialog.ShowDialog() != null) 
                tb_speicherort.Text = System.IO.Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), dialog.FileName).ToString());
            

        }

        private void textChange_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            //System.Windows.Forms.ControlExtension.Draggable(textChange, true);
        }

        private void ClrPcker_Background_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            textt.Foreground = new SolidColorBrush(cb_colors.SelectedColor.Value);
            tb_text.Foreground = new SolidColorBrush(cb_colors.SelectedColor.Value);
            
        }

        private void Thumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            UIElement thumb = e.Source as UIElement;

            Canvas.SetLeft(thumb, Canvas.GetLeft(thumb) + e.HorizontalChange);
            Canvas.SetTop(thumb, Canvas.GetTop(thumb) + e.VerticalChange);
        }

        private void b_savememe_Click(object sender, RoutedEventArgs e)
        {
          
            RenderTargetBitmap renderTargetBitmap =
            new RenderTargetBitmap(600, 510, 96, 96, PixelFormats.Pbgra32);
            renderTargetBitmap.Render(Stackpendelle);
            PngBitmapEncoder pngImage = new PngBitmapEncoder();
            pngImage.Frames.Add(BitmapFrame.Create(renderTargetBitmap));

            Microsoft.Win32.SaveFileDialog savefile = new Microsoft.Win32.SaveFileDialog();
            // set a default file name
            savefile.FileName = "neues Meme.png";
            // set filters - this can be done in properties as well
            savefile.Filter = "PNG Files(*.png)|*.png|JPG Files(*.jpg)|*.jpg|All Files (*.*)|*.*";

            if (savefile.ShowDialog() != null)
            {
                try
                {
                    using (Stream fileStream = File.Create(savefile.FileName))
                    {
                        pngImage.Save(fileStream);
                        w_memesave save = new w_memesave();
                        save.Show();
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Hoppla! Hier ist ein Fehler aufgetreten");
                    Console.WriteLine(ex.ToString());
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Hoppla! Hier ist ein Fehler aufgetreten");
            }
        }





        // für RW Drag and Drop 
        // https://www.youtube.com/watch?v=6eS1x08GzPA&t=1s
    }
}
